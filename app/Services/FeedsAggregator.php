<?php
namespace App\Services;

use App\Contracts\FeedsAggregatorContract;
use League\Flysystem\Exception;

class FeedsAggregator implements FeedsAggregatorContract
{
    /**
     * Get URL feeds
     * @param $url
     * @return mixed
     */
    public function getUrlFeeds($url)
    {
        $output = $this->getUrlOutput($url);

        return $this->parseUrlOutput($output);
    }

    /**
     * Get URL output
     *
     * @param string $url
     * @return mixed
     * @throws Exception
     */
    public function getUrlOutput($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($curl);
        curl_close($curl);

        if (empty($output))
            throw new Exception('Cannot retrieve contents of the URL provided');

        return $output;
    }

    /**
     * Parse URL xml output to array
     *
     * @param string $output
     * @return mixed
     * @throws \Exception
     */
    public function parseUrlOutput($output)
    {
        $xml = simplexml_load_string($output, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlJson = json_encode($xml);
        $array = json_decode($xmlJson, 1);

        if (!array_key_exists('title', $array['channel']['item']['0']) || !array_key_exists('link', $array['channel']['item']['0']) || !array_key_exists('description', $array['channel']['item']['0']))
            throw new \Exception('Cannot read XML format');

        return $array['channel']['item'];
    }
}
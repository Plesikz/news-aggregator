<?php
namespace App\Services;

use App\Contracts\SimpleHelperContract;

class SimpleHelper implements SimpleHelperContract
{
    /**
     * Convert date string to unix timestamp
     *
     * @param string $data
     * @return false|int
     */
    public function convertStringToTimestamp($data)
    {
        $timestamp = strtotime($data);
        if ($timestamp)
            return $timestamp;

        return strtotime(strtr($data, [
            'Sk,' => 'Sun,',
            'Pr,' => 'Mon,',
            'An,' => 'Tue,',
            'Tr,' => 'Wed,',
            'Kt,' => 'Thu,',
            'Pn,' => 'Fri,',
            'Št,' => 'Sat,',

            'Sau' => 'Jan',
            'Vas' => 'Feb',
            'Kov' => 'Mar',
            'Bal' => 'Apr',
            'Geg' => 'May',
            'Bir' => 'Jun',
            'Lie' => 'Jul',
            'Rgp' => 'Aug',
            'Rgs' => 'Sep',
            'Spa' => 'Oct',
            'Lap' => 'Nov',
            'Grd' => 'Dec',
        ]));
    }

    /**
     * Strip html tags and tabs, new lines
     *
     * @param $value
     * @return mixed
     */
    public function stripTags($value)
    {
        $value = strip_tags(preg_replace('#<a.*?>.*?</a>#i', '', $value));
        $value = strtr($value, [
            "\n" => '',
            "\t" => ''
        ]);

        return $value;
    }
}

<?php
namespace App\Providers;

use App\Contracts\FeedsAggregatorContract;
use App\Contracts\SimpleHelperContract;
use App\Services\FeedsAggregator;
use App\Services\SimpleHelper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FeedsAggregatorContract::class, FeedsAggregator::class);
        $this->app->bind(SimpleHelperContract::class, SimpleHelper::class);
    }
}
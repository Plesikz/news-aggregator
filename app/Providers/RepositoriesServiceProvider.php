<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Repositories\User\UserRepositoryInterface', 'App\Repositories\User\EloquentUserRepository');
        $this->app->bind('App\Repositories\Feed\FeedRepositoryInterface', 'App\Repositories\Feed\EloquentFeedRepository');
        $this->app->bind('App\Repositories\FeedUrl\FeedUrlRepositoryInterface', 'App\Repositories\FeedUrl\EloquentFeedUrlRepository');
    }
}
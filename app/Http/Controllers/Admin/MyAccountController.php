<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyAccountController extends Controller
{
    /**
     * View change password form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangePasswordForm()
    {
        return view('admin.myAccount.changePassword');
    }

    /**
     * Validate change password request and update user password
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        Auth::User()->password = $request->get('password');
        Auth::User()->save();

        return redirect()->route('home')->with('success', 'Password updated successfully!');
    }
}

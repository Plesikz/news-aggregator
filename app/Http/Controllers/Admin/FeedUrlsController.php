<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Repositories\FeedUrl\FeedUrlRepositoryInterface;
use Illuminate\Http\Request;

class FeedUrlsController extends Controller
{
    /**
     * View feed urls list if it's an ajax request or else show feed urls page with a list
     *
     * @param Request $request
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $items = $feedUrlRepository->all();

        return view('admin.feedUrls.'.($request->ajax() ? 'list' : 'index'), compact('items'));
    }

    /**
     * View feed urls create form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.feedUrls.create');
    }

    /**
     * Validate feed urls create request and create feed url
     *
     * @param Request $request
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $this->validateRequest($request);

        $feedUrlRepository->create([
            'title' => $request->get('title'),
            'url' => $request->get('url')
        ]);

        return redirect()->route('feed_urls.index')->with('success', 'Feed URl created successfully!');
    }

    /**
     * View feed url edit form
     *
     * @param $id
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $item = $feedUrlRepository->find($id);

        return view('admin.feedUrls.edit', compact('item'));
    }

    /**
     * Validate feed url edit request and update feed url
     *
     * @param $id
     * @param Request $request
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $this->validateRequest($request);

        $feedUrlRepository->update($id, [
            'title' => $request->get('title'),
            'url' => $request->get('url')
        ]);

        return redirect()->route('feed_urls.index')->with('success', 'Feed URl updated successfully!');
    }

    /**
     * Destroy feed url
     *
     * @param $id
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $feedUrlRepository->delete($id);

        return redirect()->route('feed_urls.index')->with('success', 'Feed URl removed successfully!');
    }

    /**
     * Validate feed url edit and create request
     *
     * @param $request
     */
    private function validateRequest($request)
    {
        $this->validate($request, [
            'title' => 'required',
            'url' => 'required|active_url'
        ]);
    }
}

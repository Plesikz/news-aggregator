<?php

namespace App\Http\Controllers;


use App\Repositories\Feed\FeedRepositoryInterface;
use App\Repositories\FeedUrl\FeedUrlRepositoryInterface;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * View latest feeds list if it's an ajax request or else show feeds page with a list
     *
     * @param Request $request
     * @param FeedRepositoryInterface $feedRepository
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request, FeedRepositoryInterface $feedRepository, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        $filter = $request->get('filter');

        $items = $feedRepository->getLatest($filter);

        $filters = array_merge(['0' => 'Show all'], $feedUrlRepository->pluck('title', 'id'));

        return view('home.'.($request->ajax() ? 'list' : 'index'), compact('items', 'filters'));
    }

    /**
     * @param $id
     * @param FeedRepositoryInterface $feedRepository
     * @return mixed
     */
    public function show($id, FeedRepositoryInterface $feedRepository)
    {
        return $feedRepository->find($id);
    }
}

<?php namespace App\Repositories;

interface EloquentRepositoryInterface
{
    public function find($id);

}
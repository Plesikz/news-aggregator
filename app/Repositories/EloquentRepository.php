<?php
namespace App\Repositories;

abstract class EloquentRepository implements EloquentRepositoryInterface
{
    protected $entity;

    public function all()
    {
        return $this->entity->all();
    }

    public function find($id)
    {
        return $this->entity->find($id);
    }

    public function pluck($title, $id)
    {
        return $this->entity->pluck($title, $id)->all();
    }

    public function create($data)
    {
        return $this->entity->create($data);
    }

    public function update($id, $data)
    {
        return $this->entity->where('id', $id)->update($data);
    }

    public function delete($id)
    {
        return $this->entity->where('id', $id)->delete();
    }

    public function whereFirst($array)
    {
        return $this->entity->where($array)->first();
    }

    public function whereDelete($array)
    {
        return $this->entity->where($array)->delete();
    }
}
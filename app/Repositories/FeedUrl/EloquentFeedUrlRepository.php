<?php
namespace App\Repositories\FeedUrl;

use App\Repositories\EloquentRepository;
use App\FeedUrl as Entity;

class EloquentFeedUrlRepository extends EloquentRepository implements FeedUrlRepositoryInterface
{
    public function __construct( Entity $entity )
    {
        $this->entity = $entity;
    }
}
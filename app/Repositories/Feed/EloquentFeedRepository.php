<?php
namespace App\Repositories\Feed;

use App\Repositories\EloquentRepository;
use App\Feed as Entity;

class EloquentFeedRepository extends EloquentRepository implements FeedRepositoryInterface
{
    public function __construct( Entity $entity )
    {
        $this->entity = $entity;
    }

    public function getLatest($feedUrlId)
    {
        $items = $this->entity
            ->select('feeds.*', 'feed_urls.title as feed_url_title')
            ->join('feed_urls', 'feeds.feed_url_id', '=', 'feed_urls.id')
            ->orderBy('feeds.publish_date', 'desc');
        if (!empty($feedUrlId))
            $items = $items->where('feed_urls.id', $feedUrlId);

        return $items->get();
    }
}
<?php
namespace App\Repositories\User;

use App\Repositories\EloquentRepository;
use App\User as Entity;

class EloquentUserRepository extends EloquentRepository implements UserRepositoryInterface
{
    public function __construct( Entity $entity )
    {
        $this->entity = $entity;
    }
}
<?php

namespace App\Console\Commands;


use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Console\Command;

class CreateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';
    /**
     * @var User
     */
    private $user;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');

        if (!empty($this->userRepository->whereFirst(['email' => $email])))
            return $this->error('Email already exists!');

        $this->userRepository->create([
            'email' => $email,
            'password' => $password,
        ]);

        return $this->info("User {$email} created successfully!");
    }
}

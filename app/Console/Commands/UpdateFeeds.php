<?php
namespace App\Console\Commands;

use App\Contracts\FeedsAggregatorContract;
use App\Contracts\SimpleHelperContract;
use App\Feed;
use App\FeedUrl;
use App\Repositories\Feed\FeedRepositoryInterface;
use App\Repositories\FeedUrl\FeedUrlRepositoryInterface;
use Illuminate\Console\Command;

class UpdateFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update latest feeds';
    /**
     * @var FeedsAggregator
     */
    private $feedsAggregator;
    /**
     * @var SimpleHelper
     */
    private $simpleHelper;
    /**
     * @var FeedRepositoryInterface
     */
    private $feedRepository;
    /**
     * @var FeedUrlRepositoryInterface
     */
    private $feedUrlRepository;


    /**
     * UpdateFeeds constructor.
     * @param FeedsAggregatorContract $feedsAggregator
     * @param SimpleHelperContract $simpleHelper
     * @param FeedRepositoryInterface $feedRepository
     * @param FeedUrlRepositoryInterface $feedUrlRepository
     */
    public function __construct(FeedsAggregatorContract $feedsAggregator, SimpleHelperContract $simpleHelper, FeedRepositoryInterface $feedRepository, FeedUrlRepositoryInterface $feedUrlRepository)
    {
        parent::__construct();
        $this->feedsAggregator = $feedsAggregator;
        $this->simpleHelper = $simpleHelper;
        $this->feedRepository = $feedRepository;
        $this->feedUrlRepository = $feedUrlRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feeds = $this->feedUrlRepository->all();

        foreach ($feeds as $feed) {
            $this->feedRepository->whereDelete(['feed_url_id' => $feed->id]);

            try {
                $items = $this->feedsAggregator->getUrlFeeds($feed->url);
            }
            catch (\Exception $e) {
                $this->error("Error occurred during \"{$feed->title}\" feed updating: \"{$e->getMessage()}\"");

                continue;
            }

            foreach ($items as $item) {
                $this->feedRepository->create([
                    'feed_url_id' => $feed->id,
                    'title' => $item['title'],
                    'description' => $this->simpleHelper->stripTags($item['description']),
                    'url' => $item['link'],
                    'publish_date' => date('Y-m-d H:i:s', $this->simpleHelper->convertStringToTimestamp($item['pubDate']))
                ]);
            }
        }

        return $this->info("Feeds updated successfully!");
    }
}

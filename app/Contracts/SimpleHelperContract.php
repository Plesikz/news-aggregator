<?php
namespace App\Contracts;

interface SimpleHelperContract
{
    /**
     * Convert date string to unix timestamp
     *
     * @param string $data
     * @return false|int
     */
    public function convertStringToTimestamp($data);

    /**
     * Strip html tags and tabs, new lines
     *
     * @param $value
     * @return mixed
     */
    public function stripTags($value);
}
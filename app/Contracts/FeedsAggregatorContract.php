<?php
namespace App\Contracts;

interface FeedsAggregatorContract
{
    /**
     * Get URL feeds
     * @param $url
     * @return mixed
     */
    public function getUrlFeeds($url);

    /**
     * Get URL output
     *
     * @param string $url
     * @return mixed
     * @throws Exception
     */
    public function getUrlOutput($url);

    /**
     * Parse URL xml output to array
     *
     * @param string $output
     * @return mixed
     * @throws \Exception
     */
    public function parseUrlOutput($output);
}
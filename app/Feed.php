<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'feed_url_id', 'title', 'description', 'url', 'publish_date'
    ];

    public $timestamps = false;

    public function feedUrl() {
        return $this->hasOne('App\FeedUrl', 'id', 'feed_url_id');
    }
}

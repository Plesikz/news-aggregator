<?php

use App\FeedUrl;
use Illuminate\Database\Seeder;

class FeedUrlsTableSeeder extends Seeder
{
    public function run()
    {
        $urls = [
            [
                'title' => 'Lrytas',
                'url' => 'http://www.lrytas.lt/rss/'
            ],
            [
                'title' => 'Delfi',
                'url' => 'http://www.delfi.lt/rss/feeds/daily.xml'
            ]
        ];

        foreach ($urls as $url)
            FeedUrl::create($url);
    }
}
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>News aggregator</title>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jumbotron-narrow.css') }}">
    </head>
    <body>

    <div class="container">
        <div class="header clearfix">
            <nav>
                <ul class="nav nav-pills pull-right">
                    <li role="presentation" class="active"><a href="{{ route('home') }}">Latest feeds</a></li>
                    @if (Auth::check())
                        <li role="presentation"><a href="{{ route('feed_urls.index') }}">Manage feeds</a></li>
                        <li role="presentation"><a href="{{ route('my_account.change_password') }}">Change password</a></li>
                        <li role="presentation"><a href="{{ route('logout') }}">Logout</a></li>
                    @else
                        <li role="presentation"><a href="{{ route('login') }}">Login</a></li>
                    @endif
                </ul>
            </nav>
            <h3 class="text-muted">News aggregator</h3>
        </div>
        @if (isset($errors) && count($errors))
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @yield('content')

        <footer class="footer">
            <p>&copy; 2016 Company, Inc.</p>
        </footer>
    </div> <!-- /container -->
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>

    @yield('javascript')
    </body>
</html>

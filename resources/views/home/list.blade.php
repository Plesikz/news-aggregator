@foreach($items as $item)
    <li>[{{ $item->feed_url_title }}] <a href="{{ route('feed', [$item->id]) }}" class="feed-modal-open">{{ $item->title }}</a> [{{ $item->publish_date }}]</li>
@endforeach

@extends('layout')

@section('content')
    <p>
        <select name="filter" class="form-control">
            @foreach($filters as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </p>
    <ul id="feeds-list">
        @include('home.list')
    </ul>

    <!-- Modal -->
    <div class="modal fade" id="feedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close the modal</button>
                    <a href="#" class="btn btn-primary modal-link" target="_blank">Go to feed page</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('select[name="filter"]').on('change', function () {
                var val = $(this).val();
                $.ajax({
                    url: "{{ route('home') }}",
                    data: {
                        filter: val
                    },
                    beforeSend: function () {
                        $("#feeds-list").css('opacity', 0.5);
                    },
                    success: function(result){
                        $("#feeds-list").html(result).css('opacity', 1);
                    }
                });
            });

            $(document).on('click', '.feed-modal-open', function (e) {
                e.preventDefault();

                var el = $(this);
                var url = el.attr('href');
                var mod = $('#feedModal');
                $.ajax({
                    url: url,
                    success: function(result){
                        mod.find('.modal-title').html(result.title);
                        mod.find('.modal-body').html(result.description);
                        mod.find('.modal-link').attr('href', result.url);
                        console.log(result.url);
                        mod.modal('show');
                    }
                });
            });
        });
    </script>
@stop
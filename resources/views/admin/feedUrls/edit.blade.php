@extends('layout')

@section('content')
    <form action="{{ route('feed_urls.update', [$item->id]) }}" method="POST">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group">
            <label for="inputTitle">Title</label>
            <input name="title" type="text" id="inputTitle" class="form-control" value="{{ $item->title }}" required autofocus>
        </div>
        <div class="form-group">
            <label for="inputUrl">URL</label>
            <input name="url" type="text" id="inputUrl" class="form-control" value="{{ $item->url }}" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Edit</button>
    </form>
@stop
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Title</th>
            <th>URL</th>
            <th>Actions</th>
        </tr>
    </thead>
    <thead>
        @foreach($items as $item)
            <tr>
                <td>{{ $item->title }}</td>
                <td>{{ $item->url }}</td>
                <td><a href="{{ route('feed_urls.edit', [$item->id]) }}" class="btn btn-warning btn-xs">Edit</a> <a href="{{ route('feed_urls.destroy', [$item->id]) }}" class="btn btn-danger btn-xs">Delete</a></td>
            </tr>
        @endforeach
    </thead>
</table>
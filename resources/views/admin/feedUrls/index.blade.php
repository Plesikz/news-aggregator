@extends('layout')

@section('content')
    <div>
        <a href="{{ route('feed_urls.create') }}" class="btn btn-success">Add feed URL</a>
    </div>
    <div id="feeds-list">
        @include('admin.feedUrls.list')
    </div>
@stop
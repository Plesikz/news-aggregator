@extends('layout')

@section('content')
    <form action="{{ route('feed_urls.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="inputTitle">Title</label>
            <input name="title" type="text" id="inputTitle" class="form-control" required autofocus>
        </div>
        <div class="form-group">
            <label for="inputUrl">URL</label>
            <input name="url" type="text" id="inputUrl" class="form-control" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
    </form>
@stop
@extends('layout')

@section('content')
    <form action="{{ route('my_account.change_password') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="inputPassword">Password</label>
            <input name="password" type="password" id="inputPassword" class="form-control" required autofocus>
        </div>
        <div class="form-group">
            <label for="inputPasswordConfirmation">Password confirmation</label>
            <input name="password_confirmation" type="password" id="inputPasswordConfirmation" class="form-control" required>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
    </form>
@stop
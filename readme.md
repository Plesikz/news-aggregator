# News Aggregator

News aggregator is a small apllication that collects rss feeds and aranges them by timestamp.

## Installation instructions

Run this to install composer dependencies
```
composer install
```
Run this to migrate database
```
php artisan migrate
```

## Example usage

Run this to create user
```
php artisan user:create test@example.com password
```
Run this to update news feed
```
php artisan feed:update
```

## Authors

Marius Daunoravičius

## License

The News aggregator is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/feed/{id}', ['as' => 'feed', 'uses' => 'HomeController@show']);

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login.submit', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout', 'middleware' => ['auth'], 'uses' => 'Auth\LoginController@logout']);

# Admin
Route::group(['middleware' => ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function ()
{
    Route::resource('feed_urls', 'FeedUrlsController', ['except' => ['show', 'destroy']]);
    Route::get('feed_urls/{id}/destroy', ['as' => 'feed_urls.destroy', 'uses' => 'FeedUrlsController@destroy']);

    Route::get('my_account/change_password', ['as' => 'my_account.change_password_form', 'uses' => 'MyAccountController@showChangePasswordForm']);
    Route::post('my_account/change_password', ['as' => 'my_account.change_password', 'uses' => 'MyAccountController@changePassword']);
});

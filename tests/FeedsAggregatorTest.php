<?php

class FeedsAggregatorTest extends TestCase
{
    /**
     * @test
     */
    public function it_throws_an_exception_if_it_cant_retrieve_url_content()
    {
        $this->setExpectedException('Exception');
        $feedsAggregator = $this->app->make('App\Contracts\FeedsAggregatorContract');
        $feedsAggregator->getUrlOutput('http://test');

    }

    /**
     * @test
     */
    public function it_throws_an_exception_if_xml_dont_contain_required_structure()
    {
        $this->setExpectedException('Exception');
        $feedsAggregator = $this->app->make('App\Contracts\FeedsAggregatorContract');
        $feedsAggregator->parseUrlOutput('<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0"></rss>');
    }

    /**
     * @test
     */
    public function it_returns_an_array_of_feeds()
    {
        $feedsAggregator = $this->app->make('App\Contracts\FeedsAggregatorContract');
        $feeds = $feedsAggregator->getUrlFeeds('http://www.15min.lt/rss');
        $this->assertArrayHasKey(0, $feeds);
    }
}

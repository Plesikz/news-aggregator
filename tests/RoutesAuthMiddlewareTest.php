<?php


class RoutesAuthMiddlewareTest extends TestCase
{
    /**
     * @test
     */
    public function it_redirects_to_the_login_page_if_user_is_not_logged_in()
    {
        $response = $this->call('GET', route('feed_urls.index'));
        $response->isRedirection();
        $this->assertRedirectedTo(route('login'));

        $response = $this->call('GET', route('feed_urls.destroy', [1]));
        $response->isRedirection();
        $this->assertRedirectedTo(route('login'));

        $response = $this->call('GET', route('my_account.change_password_form'));
        $response->isRedirection();
        $this->assertRedirectedTo(route('login'));

        $response = $this->call('GET', route('my_account.change_password'));
        $response->isRedirection();
        $this->assertRedirectedTo(route('login'));
    }
}

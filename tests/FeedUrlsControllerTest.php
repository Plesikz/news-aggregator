<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;

class FeedUrlsControllerTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * @test
     */
    public function it_shows_the_feed_urls_index_page()
    {
        $response = $this->call('GET', route('feed_urls.index'));
        $this->assertTrue($response->isOk());
    }

    /**
     * @test
     */
    public function it_shows_the_feed_url_create_page()
    {
        Auth::shouldReceive('authenticate')->shouldReceive('check');

        $response = $this->call('GET', route('feed_urls.create'));
        $this->assertTrue($response->isOk());
    }

    /**
     * @test
     */
    public function it_redirects_to_the_feed_url_create_page_if_user_dont_fill_in_fields()
    {
        $response = $this->call('POST', route('feed_urls.store'));
        $this->assertSessionHasErrors('title');
        $this->assertSessionHasErrors('url');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * @test
     */
    public function it_redirects_to_the_feed_urls_index_page_if_user_fills_in_fields()
    {
        $data = ['title' => 'test', 'url' => 'http://15min.lt'];
        $response = $this->call('POST', route('feed_urls.store'), $data);
        $this->assertSessionHas('success');
        $this->assertTrue($response->isRedirection());
        $this->assertNotEmpty(\App\FeedUrl::where($data)->first());
    }

    /**
     * @test
     */
    public function it_shows_the_feed_url_edit_page()
    {
        Auth::shouldReceive('authenticate')->shouldReceive('check');

        $response = $this->call('GET', route('feed_urls.edit', [1]));
        $this->assertTrue($response->isOk());
    }

    /**
     * @test
     */
    public function it_redirects_to_the_feed_url_edit_page_if_user_dont_fill_in_fields_create_page()
    {
        $response = $this->call('PUT', route('feed_urls.update', [1]));
        $this->assertSessionHasErrors('title');
        $this->assertSessionHasErrors('url');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * @test
     */
    public function it_redirects_to_the_feed_urls_index_page_if_user_fills_in_fields_edit_page()
    {
        $data = ['title' => 'test', 'url' => 'http://15min.lt'];
        $response = $this->call('PUT', route('feed_urls.update', [1]), $data);
        $this->assertSessionHas('success');
        $this->assertTrue($response->isRedirection());
        $this->assertNotEmpty(\App\FeedUrl::where($data)->first());
    }

    /**
     * @test
     */
    public function it_redirects_to_the_feed_urls_index_page_after_feed_url_deletion()
    {
        $response = $this->call('GET', route('feed_urls.destroy', [1]));
        $this->assertSessionHas('success');
        $this->assertTrue($response->isRedirection());
    }
}

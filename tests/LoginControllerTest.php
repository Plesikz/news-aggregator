<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;

class LoginControllerTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * @test
     */
    public function it_shows_the_login_form()
    {
        $response = $this->call('GET', route('login'));
        $this->assertTrue($response->isOk());
    }

    /**
     * @test
     */
    public function it_redirects_back_to_login_form_if_user_dont_fill_in_fields()
    {
        $response = $this->call('POST', route('login.submit'));
        $this->assertSessionHasErrors('email');
        $this->assertSessionHasErrors('password');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * @test
     */
    public function it_redirects_back_to_login_form_if_authentication_fails()
    {
        $credentials = ['email' => 'users@example.com', 'password' => 'password'];

        Auth::shouldReceive('attempt')->once()->with($credentials, false)->andReturn(false);
        $response = $this->call('POST', route('login.submit'), $credentials);
        $this->assertSessionHasErrors('email');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * @test
     */
    public function it_redirects_to_home_page_after_user_logs_in()
    {
        $credentials = ['email' => 'users@example.com', 'password' => 'password'];

        Auth::shouldReceive('attempt')->once()->with($credentials, false)->andReturn(true);

        $response = $this->call('POST', route('login.submit'), $credentials);
        $this->assertTrue($response->isRedirection());
        $this->assertRedirectedTo(route('home'));
    }
}

<?php

use App\User;

class HomeControllerTest extends TestCase
{
    /**
     * @test
     */
    public function it_shows_login_button_if_user_is_not_logged_in()
    {
        $this->visit('/')
            ->see(route('login'))
            ->dontSee(route('feed_urls.index'));
    }

    /**
     * @test
     */
    public function it_dont_show_login_button_if_user_is_logged_in()
    {
        $user = new User(array('name' => 'John'));
        $this->be($user);

        $this->visit('/')
            ->dontSee(route('login'))
            ->see(route('feed_urls.index'));
    }
}

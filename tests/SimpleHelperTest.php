<?php

class SimpleHelperTest extends TestCase
{
    /**
     * @test
     */
    public function it_converts_date_string_to_unix_timestamp()
    {
        $simpleHelper = $this->app->make('App\Contracts\SimpleHelperContract');

        $date = 'Wed, 26 Oct 2016 17:33:27 +0300';
        $timestamp = $simpleHelper->convertStringToTimestamp($date);
        $this->assertTrue(is_int($timestamp));

        $date = 'An, 26 Spa 2016 17:33:27 +0300';
        $timestamp = $simpleHelper->convertStringToTimestamp($date);
        $this->assertTrue(is_int($timestamp));

        $date = 'test';
        $simpleHelper = $this->app->make('App\Contracts\SimpleHelperContract');
        $timestamp = $simpleHelper->convertStringToTimestamp($date);
        $this->assertFalse(is_int($timestamp));
    }

    /**
     * @test
     */
    public function it_removes_html_tags_and_line_breaks()
    {
        $simpleHelper = $this->app->make('App\Contracts\SimpleHelperContract');

        $text = "\n<a href=\"#\">Hello</a>Test\n\t\t";
        $cleanText = $simpleHelper->stripTags($text);
        $this->assertEquals('Test', $cleanText);
    }
}
